import caffe
import numpy

print('PyCaffe is available at version {}'.format(caffe.__version__))
print('Numpy is available at version {}'.format(numpy.__version__))