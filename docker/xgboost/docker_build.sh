if [ $(dpkg-query -W -f='${Status}' nvidia-docker 2>/dev/null | grep -c "installed") -eq 0 ];
then
  apt-get install nvidia-modprobe;
  apt-get install nvidia-docker;
fi

if [ -z $(nvidia-docker images -q personal/xgboost) ]; 
then
    echo "Image does not exist, building it"
    nvidia-docker build -t personal/xgboost . --no-cache
fi

nvidia-docker run --rm personal/xgboost "$@"
